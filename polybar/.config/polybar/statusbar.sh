#!/usr/bin/bash

## script for toggling status bar(polybar)
if [[ -n $(pgrep polybar) ]]; then
echo "killing status bar"
pkill polybar

else
echo "activating status bar"
~/.config/polybar/launch.sh

fi